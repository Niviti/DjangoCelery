



from .views import progress_view

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.urls import path,include

app_name="billing"

urlpatterns = [
    url('res', progress_view, name="task_status")
] 

##if settings.DEBUG:
##    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)