from django.db import models







class BillingItem(models.Model):
    number1   = models.IntegerField(default="5")
    number2   = models.IntegerField(default="6")
    total      = models.IntegerField(default="7")
    item_name  = models.CharField(max_length=120, null=True, blank=True)
    timestamp  = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
    	return "{total}".format(total=self.total)

    def __str__(self):
        return "{total}".format(total=self.total) 	