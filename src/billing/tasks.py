from __future__ import absolute_import, unicode_literals
import random
from celery.decorators import task


from .models import BillingItem
from celery import shared_task
from celery_progress.backend import ProgressRecorder
import time

@task(name="sum_two_numbers")
def add(x, y):
    return x + y


@task(name="multiply_two_numbers")
def mul(x, y):
    number1 = x
    number2 = y * random.randint(3, 100)

    total = number1 * number2

    new_obj = BillingItem.objects.create(
    	item_name='some item',
    	number1=number1,
    	number2=number2, 
    	total=total
    	)
    return total


@task(name="sum_list_numbers")
def xsum(numbers):
    return sum(numbers)

@task()
def abc(numbers):
    return sum(numbers)

@shared_task(bind=True)
def my_task(self, seconds):
    progress_recorder = ProgressRecorder(self)
    for i in range(seconds):
        time.sleep(1)
        progress_recorder.set_progress(i + 1, seconds)
    return 'done'