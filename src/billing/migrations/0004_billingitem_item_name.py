# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-02 18:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0003_billingitem_total'),
    ]

    operations = [
        migrations.AddField(
            model_name='billingitem',
            name='item_name',
            field=models.CharField(blank=True, max_length=120, null=True),
        ),
    ]
