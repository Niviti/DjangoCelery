
from .tasks import my_task
from django.shortcuts import render,redirect


def progress_view(request):
    result = my_task.delay(10)
    return render(request, 'display_progress.html', context={'task_id': result.task_id})


