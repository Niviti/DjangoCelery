
from django.conf.urls import url
from django.contrib import admin
from django.urls import path,include
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('bil/', include('billing.urls',namespace="celery_progress"))
]
